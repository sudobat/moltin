set xlabel "pas de temps (k)"
set ylabel "Energia"
plot "energia.dat" u 1:2 t "Epot", "energia.dat" u 1:3 t "Ecin", "energia.dat" u 1:4 t "ETOT"
set term png
set output "fit.png"
replot
quit
