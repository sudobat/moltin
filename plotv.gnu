set xlabel "pas de temps (k)"
set ylabel "vcm"
plot "vcm.dat" u 1:2 t "vcmx", "vcm.dat" u 1:3 t "vcmy", "vcm.dat" u 1:4 t "vcmz"
set term png
set output "vcm.png"
replot
quit
