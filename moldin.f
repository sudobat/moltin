	program moldin
	implicit none
	real*8 To, tmax, t, delta, L
	real*8 Epot(6000), Ecin(6000), En(6000), Epotcut, rcut,rs,a,b
	real*8 x(6000,6000),  y(6000,6000), z(6000,6000)
	real*8 vx(6000,6000), vy(6000,6000), vz(6000,6000)
	real*8 fx(6000,6000), fy(6000,6000), fz(6000,6000)
	real*8 pex(6000)
	real*8 dmin2, mass, Vol, rho, pres
	integer*8 i, kmax, k, anderson, num, N, Nmin, Nmax, Ninc
	integer*8 o, omax
	open (32,file="energia.dat")
	open (10,file="vcm.dat")
	open (12,file="fx.dat")
	open (24,file="pex.dat")
	open (93,file="pres.dat")
C       --------------------------Paràmetres-------------------------
	write(*,*) "Input:"

	call readconfig(mass,N,L,delta,To,tmax,rho,dmin2,anderson)

	write(*,*) "Do you want Anderson? (1=yes, 0=no)"
	read(*,*) anderson

	write(*,*) "mass", 	mass
	write(*,*) "N",	N
	write(*,*) "L",	L
	write(*,*) "delta", delta
	write(*,*) "To",	To
	write(*,*) "tmax",	tmax
	write(*,*) "rho",	rho
	write(*,*) "dmin2",	dmin2
	write(*,*) "Anderson", anderson
	write(*,*) "-------------"

C 	----------------Density loop-----------------------------
	Ninc = 0
	Nmin = N
	Nmax = Nmin + Ninc

	do N=Nmin,Nmax,100

	t=0.0d0

C   	time steps:
	kmax=nint(tmax/delta)+1
	write(*,*) "kmax", kmax
	
	call init(To,N,x,y,z,vx,vy,vz,fx,fy,fz,pex,
     & L,dmin2,Epot,Epotcut,rcut,rs,a,b)

	write(*,*) "-------------"
	
	write(*,*) "Start computing"
	pres=0.0d0
	num=0
	k=2
	do while (k.le.kmax)
C	write(*,*) "k", k
	call integrate(k,x,y,z,vx,vy,vz,delta,fx,fy,fz,pex,
     & mass,N,L,Ecin,Epot,En,Epotcut,rcut,rs,a,b,anderson)
	pex(k)=(1.0d0/((3.0d0)*L*L*L))*pex(k)
		if (N.eq.Nmin) then
			write(32,*) k, Epot(k), Ecin(k), En(k)
		end if

		if (anderson.eq.1) then
C			if (N.eq.Nmin) then
			if (N.eq.Nmin) then
				write(24,*) k, pex(k)
			end if

			if (dble(k).gt.dble(kmax)/2.0d0) then
	pres=pres+pex(k)+(1.0d0/(L*L*L))*(2.0d0/3.0d0)*Ecin(k)
				num=num+1			
			end if
		end if

		t=t+delta
		k=k+1
	enddo

	pres=pres/dble(num)
	rho=dble(N)/(L**3)
	write(93,*) rho, pres
	write(*,*) rho, pres

		if ((N.eq.Nmin).and.(anderson.eq.1)) then
			call radial(x,y,z,rho,rcut,kmax,N,L)
		end if

		write(*,*) "N:", N
	if (anderson.ne.1) then
		goto 1314
	end if

	end do
1314	continue	
C 	------------------End of the density loop ----------------
																																							
	close(32)
	close(10)
	close(12)
	close(24)
C	close(93)

	end



C       ----------------Integator: Velocity Verlet------------------
	subroutine integrate(k,x,y,z,vx,vy,vz,delta,fx,fy,fz,pex,
     & mass,N,L,Ecin,Epot,En,Epotcut,rcut,rs,a,b,anderson)
	implicit none 

	real*8 Ecin(6000), Epot(6000), En(6000)
	real*8 Epotcut, rcut, rs, a, b
	real*8 mass, delta, L, fxtotal
	integer*8 N, k, i, anderson
	real*8 x(6000,6000), y(6000,6000), z(6000,6000)
	real*8 vx(6000,6000), vy(6000,6000), vz(6000,6000)
	real*8 fx(6000,6000), fy(6000,6000), fz(6000,6000)
	real*8 pex(6000)
	real*8 vcmx, vcmy, vcmz
	real*8 gaussian_x, gaussian_y, gaussian_z
	real*8 anderson_probability, T_anderson
	real*8 pi

	pi=4.0d0*datan(1.0d0)
	
	T_anderson=2.0d0
	anderson_probability=0.20d0

	Epot(k)=0.0d0
	Ecin(k)=0.0d0
	vcmx=0.0d0
	vcmy=0.0d0
	vcmz=0.0d0
	fxtotal=0.0d0
	do i=1,N
	x(i,k)=x(i,k-1)+delta*vx(i,k-1)+delta*delta*fx(i,k-1)/(2.0d0*mass)
	y(i,k)=y(i,k-1)+delta*vy(i,k-1)+delta*delta*fy(i,k-1)/(2.0d0*mass)
	z(i,k)=z(i,k-1)+delta*vz(i,k-1)+delta*delta*fz(i,k-1)/(2.0d0*mass)

C 		PBC
		if(x(i,k).lt.(-L/2.0d0)) then
			x(i,k)=x(i,k)+L
		else if(x(i,k).gt.(L/2.0d0)) then
			x(i,k)=x(i,k)-L
		end if		

		if(y(i,k).lt.(-L/2.0d0)) then
			y(i,k)=y(i,k)+L
		else if(y(i,k).gt.(L/2.0d0)) then
			y(i,k)=y(i,k)-L
		end if

		if(z(i,k).lt.(-L/2.0d0)) then
			z(i,k)=z(i,k)+L
		else if(z(i,k).gt.(L/2.0d0)) then
			z(i,k)=z(i,k)-L
		end if

	end do
	pex(k)=0.0d0
	do i=1,N
	call force(i,k,x,y,z,fx,fy,fz,pex,N,Epot,Epotcut,rcut,rs,a,b,L)

	vx(i,k)=vx(i,k-1)+((fx(i,k)+fx(i,k-1))/(2.0d0*mass))*delta
	vy(i,k)=vy(i,k-1)+((fy(i,k)+fy(i,k-1))/(2.0d0*mass))*delta
	vz(i,k)=vz(i,k-1)+((fz(i,k)+fz(i,k-1))/(2.0d0*mass))*delta

C 	Anderson thermostat
		if (anderson.eq.1) then
	gaussian_x = sqrt(-1.0d0*log(rand())) * cos(2.0d0*pi*rand())
	gaussian_y = sqrt(-1.0d0*log(rand())) * cos(2.0d0*pi*rand())
	gaussian_z = sqrt(-1.0d0*log(rand())) * cos(2.0d0*pi*rand())


			if (rand().le.anderson_probability) then
				
				vx(i,k)=T_anderson/mass * gaussian_x
				vy(i,k)=T_anderson/mass * gaussian_y
				vz(i,k)=T_anderson/mass * gaussian_z

			end if

		end if
		Ecin(k)=Ecin(k)+
     &    (mass/2.0d0)*(vx(i,k)**2+vy(i,k)**2+vz(i,k)**2)
		
C	To Validate the code
		fxtotal=fxtotal+fx(i,k)
		vcmx=vcmx+vx(i,k)
		vcmy=vcmy+vy(i,k)
		vcmz=vcmz+vz(i,k)


	end do

		write(10,*) k, vcmx, vcmy, vcmz
		write(12,*) k, fxtotal
		
C 		To avoid double counting
	Epot(k)=Epot(k)/2.0d0

	En(k)=Epot(k)+Ecin(k)
	
		
	end

C       -------------------------Initialization----------------------------
	subroutine init(To,N,x,y,z,vx,vy,vz,fx,fy,fz,pex,
     & L,dmin2,Epot,Epotcut,rcut,rs,a,b)
	implicit none 
	real*8 Tempx, Tempy, Tempz, To
	real*8 delta, L, xr, yr, zr
	real*8 x(6000,6000), y(6000,6000), z(6000,6000)
	real*8 vx(6000,6000), vy(6000,6000), vz(6000,6000)
	real*8 fx(6000,6000), fy(6000,6000), fz(6000,6000)
	real*8 pex(6000)
	real*8 vcmx, vcmy, vcmz, vcx, vcy, vcz
	real*8 Epot(6000), Epotcut, rcut, rs, a, b
	real*8 dmin2, drel2
	integer*4 validpos
	integer*8 N, i, k,j

	k=1

	rcut=2.5d0
	rs=(48.0d0/67.0d0)*rcut
	
	b = -48/(rs**13 * (rs-rcut)**2)
     &	+24/(rs**7 * (rs-rcut)**2)
     &	-8/(rs**11 * (rs-rcut)**3)
     &	+8/(rs**5 * (rs-rcut)**3)
	
	a = -b*(rs-rcut)
     &	+4.0d0/(rs**12 * (rs-rcut)**2)
     &	-4.0d0/(rs**6 * (rs-rcut)**2)

	Epotcut=4.0d0*(1.0d0/rcut**12-1.0d0/rcut**6)

	call srand(1505)

	Epot=0.0d0

	do i=1,N
		validpos=0
		do while(validpos.eq.0)
			validpos=1
C       	Particles in a box [-L/2,L/2]
			x(i,1)=L*(rand()-0.5d0)
			y(i,1)=L*(rand()-0.5d0)
			z(i,1)=L*(rand()-0.5d0)
			do j=1,i-1
				xr=x(i,1)-x(j,1)
				yr=y(i,1)-y(j,1)
				zr=z(i,1)-z(j,1)

				xr=xr-L*nint(xr/L)
				yr=yr-L*nint(yr/L)
				zr=zr-L*nint(zr/L)
				drel2=xr**2+yr**2+zr**2
				if (drel2.lt.dmin2) then
					validpos=0
					goto 666
C       		It is no longer valid, we stop the loop
				endif
			enddo
666 		continue
		enddo
		vx(i,1)=rand()-0.5d0
		vy(i,1)=rand()-0.5d0
		vz(i,1)=rand()-0.5d0

	end do

	pex=0.0d0	
	do i=1,N
	call force(i,k,x,y,z,fx,fy,fz,pex,N,Epot,Epotcut,rcut,rs,a,b,L)
	
	end do

C 	To avoid double counting
	Epot(1)=Epot(1)/2.0d0
C	write(*,*) "Epot", Epot(1)

C 	We set the total vcm to zero
	vcmx=0.0d0
	vcmy=0.0d0
	vcmz=0.0d0

	vcx=0.0d0
	vcy=0.0d0
	vcz=0.0d0

	Tempx=0.0d0
	Tempy=0.0d0
	Tempz=0.0d0

	do i=1,N
		vcmx=vcmx+vx(i,1)
		vcmy=vcmy+vy(i,1)
		vcmz=vcmz+vz(i,1)

		Tempx=Tempx+vx(i,1)**2/2.0d0
		Tempy=Tempy+vy(i,1)**2/2.0d0
		Tempz=Tempz+vz(i,1)**2/2.0d0
	end do

	vcmx=vcmx/dble(N)
	vcmy=vcmy/dble(N)
	vcmz=vcmz/dble(N)


	Tempx=Tempx/dble(N)
	Tempy=Tempy/dble(N)
	Tempz=Tempz/dble(N)


	do i=1,N
		vx(i,1)=vx(i,1)-vcmx
		vy(i,1)=vy(i,1)-vcmy
		vz(i,1)=vz(i,1)-vcmz



		vx(i,1)=sqrt(To/Tempx)*vx(i,1)
		vy(i,1)=sqrt(To/Tempy)*vy(i,1)
		vz(i,1)=sqrt(To/Tempz)*vz(i,1)

		vcx=vcx+vx(i,1)
		vcy=vcy+vy(i,1)
		vcz=vcz+vz(i,1)

	end do

	end

C       -------- To compute the force and the potential energy--------

	subroutine force(i,k,x,y,z,fx,fy,fz,pex,
     & N,Epot,Epotcut,rcut,rs,a,b,L)
	implicit none 
	real*8 Epot(6000), Epotcut, rcut, rcut2, rs, rs2, a, b
	real*8 xr, yr, zr, r6i, r, r2, r2i, ff
	real*8 x(6000,6000), y(6000,6000), z(6000,6000)
	real*8 fx(6000,6000), fy(6000,6000), fz(6000,6000)
	real*8 pex(6000)
	real*8 rho, L
	integer*8 N, k, i, j
	real*8 mindist2

C	pex(k)=0.0d0

	mindist2=0.0d0
	rcut2=rcut**2
	rs2=rs**2

	fx(i,k)=0.0d0
	fy(i,k)=0.0d0
	fz(i,k)=0.0d0

	do j=1,N
	
		if (j.ne.i) then
			xr=x(i,k)-x(j,k)
			yr=y(i,k)-y(j,k)
			zr=z(i,k)-z(j,k)

			xr=xr-L*nint(xr/L)
			yr=yr-L*nint(yr/L)
			zr=zr-L*nint(zr/L)

			r2=xr**2+yr**2+zr**2
			r=sqrt(r2)

			if (r2.lt.rs2) then
				r2i=1.0d0/r2
				r6i=r2i*r2i*r2i

			ff=48.0d0*r2i*r6i*(r6i-1.0d0/2.0d0)
			fx(i,k)=fx(i,k) + ff*xr
			fy(i,k)=fy(i,k) + ff*yr
			fz(i,k)=fz(i,k) + ff*zr
			pex(k)=pex(k)+r2*ff
C 	The this must be divided by 2 to avoid double counting
			Epot(k)=Epot(k) + 4*r6i*(r6i-1) - Epotcut
			else if ((rs2.le.r2).and.(r2.lt.rcut2)) then
			ff= -2.0d0*a*(r-rcut)/r - 3.0d0*b*(r-rcut)**2/r
			fx(i,k)=fx(i,k) + ff*xr
			fy(i,k)=fy(i,k) + ff*yr
			fz(i,k)=fz(i,k) + ff*zr
			pex(k)=pex(k)+r2*ff
	Epot(k)=Epot(k) + a*(r-rcut)**2 + b*(r-rcut)**3 - Epotcut
 			end if

C			pex(k)=pex(k)+r2*ff
 			end if
		
	enddo
	pex(k)=pex(k)/2.0d0
	end



	subroutine radial(x,y,z,rho,rcut,kmax,N,L)
	implicit none 
	real*8 x(6000,6000), y(6000,6000), z(6000,6000)
	real*8 rho, r, deltar, rmax, rcut, xr, yr, zr
	real*8 g(6000), Vrdr, pi,rdr2, dist2, r2, gtot(6000)
	integer*8 Nrdr, qmax, q
	integer*8 kmax, N, i, j

	integer*8 k

	real*8 L
	
	write(*,*) "------------------------------"	
	write(*,*) " Radial disrtibution function "
	write(*,*) "------------------------------"
	open (11,file="g.dat")
	pi=4.0d0*datan(1.0d0)

	rmax=L/2.0d0
	r=0.0d0
	deltar=0.1d0

	q=1
	qmax=nint(rmax/deltar)+1

	write(*,*) "N", N
	write(*,*) "rmax", rmax
	write(*,*) "qmax", qmax
	write(*,*) "rho", rho

	do while(q.le.qmax)

C	do k=nint(2.0d0*dble(kmax)/3.0d0),kmax
		Nrdr=0.0d0
		r2=r**2
		rdr2=(r+deltar)**2
		do i=1,N
			do j=1,N
				if (i.ne.j) then

					xr = x(j,kmax)-x(i,kmax)
					yr = y(j,kmax)-y(i,kmax)
					zr = z(j,kmax)-z(i,kmax)

					xr=xr-L*nint(xr/L)
					yr=yr-L*nint(yr/L)
					zr=zr-L*nint(zr/L)

					dist2=xr**2+yr**2+zr**2

				if ((dist2.ge.r2).and.(dist2.le.rdr2)) then
					Nrdr=Nrdr+1
				end if

				end if
			end do
		end do

	Vrdr=(4.0d0*pi)/3.0d0*((r+deltar/2.0d0)**3-(r-deltar/2.0d0)**3)

		g(q)=Nrdr/(rho*Vrdr*N)
C		g(q)=g(q) + Nrdr/(rho*Vrdr*N)
C	end do	
C	g(q)=g(q)/dble(kmax - nint(2.0d0*dble(kmax)/3.0d0))
		write(11,*) q, g(q)

		r=r+deltar
		q=q+1
	end do

	close(11)

	end

C 	To read the input
	subroutine readconfig(mass,N,L,delta,To,tmax,rho,dmin2,anderson)
	implicit none 
	real*8 To, tmax, delta, L
	real*8 dmin2, mass, rho
	integer*8 N, anderson

	open(unit=1, file="config.txt")
	read(1,*) mass
	read(1,*) N
	read(1,*) L
	read(1,*) delta
	read(1,*) To
	read(1,*) tmax
	close(1)
	rho=dble(N)/(L*L*L)
	dmin2=0.75d0**2
	end
